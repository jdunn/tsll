# TypeScript Linked List

A simple linked list implementation in TypeScript 4.

## Pre-Requisites

Tested on macOS 10.15.6 with:

* Node v11.3.0
* npm v6.14.5
* TypeScript 4.0.5 (as you can see in package.json)

## Installation

`npm i`


## Running

You might wish to run tsc to ensure you have the latest output files:

`npm run build`

Then you can run:

`npm run main`

## Testing

`npm run test`

## Interesting Files

If you want to see the TypeScript, check out [./src](./src).  If you want to see the unit tests, they are under [./spec/list](./spec/list).