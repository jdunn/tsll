"use strict";
/**
 * Type definitions and class for a linked list.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LinkedList = void 0;
var LinkedList = /** @class */ (function () {
    function LinkedList() {
        this.nodes = {};
        this.idFirst = null;
        this.idLast = null;
    }
    LinkedList.prototype.length = function () {
        return Object.keys(this.nodes).length;
    };
    LinkedList.prototype.toString = function () {
        var n = this.first();
        var s = "\n----------------------------\n";
        while (n) {
            s += JSON.stringify(n.content) + "\n";
            n = this.next(n);
        }
        return s;
    };
    LinkedList.prototype.putBetween = function (prev, id, next) {
        this.nodes[id].meta.prev = prev;
        this.nodes[id].meta.next = next;
        if (next) {
            this.nodes[next].meta.prev = id;
        }
        else {
            this.idLast = id;
        }
        if (prev) {
            this.nodes[prev].meta.next = id;
        }
        else {
            this.idFirst = id;
        }
    };
    LinkedList.prototype.insertAfter = function (after, item) {
        this.nodes[item.meta.id] = item;
        this.putBetween(after.meta.id, item.meta.id, after.meta.next);
        return item.meta.id;
    };
    LinkedList.prototype.insertBefore = function (item, before) {
        this.nodes[item.meta.id] = item;
        this.putBetween(before.meta.prev, item.meta.id, before.meta.id);
        return item.meta.id;
    };
    LinkedList.prototype.addOnly = function (item) {
        this.nodes[item.meta.id] = item;
        this.idFirst = item.meta.id;
        this.idLast = item.meta.id;
        return this.idFirst;
    };
    /**
     * "Back" of the list (amusement park line) means after the last one.
     */
    LinkedList.prototype.addToBack = function (item) {
        if (this.idLast) {
            return this.insertAfter(this.nodes[this.idLast], item);
        }
        return this.addOnly(item);
    };
    LinkedList.prototype.addToFront = function (item) {
        if (this.idFirst) {
            return this.insertBefore(item, this.nodes[this.idFirst]);
        }
        return this.addOnly(item);
    };
    LinkedList.prototype.remove = function (item) {
        if (!this.nodes.hasOwnProperty(item.meta.id)) {
            return false;
        }
        if (item.meta.prev) {
            this.nodes[item.meta.prev].meta.next = item.meta.next;
        }
        else {
            this.idFirst = item.meta.next;
        }
        if (item.meta.next) {
            this.nodes[item.meta.next].meta.prev = item.meta.prev;
        }
        else {
            this.idLast = item.meta.prev;
        }
        delete this.nodes[item.meta.id];
        return true;
    };
    LinkedList.prototype.first = function () {
        if (this.idFirst) {
            return this.nodes[this.idFirst];
        }
        return null;
    };
    LinkedList.prototype.last = function () {
        if (this.idLast) {
            return this.nodes[this.idLast];
        }
        return null;
    };
    LinkedList.prototype.next = function (item) {
        if (!item) {
            return null;
        }
        if (!item.meta.next) {
            return null;
        }
        return this.nodes[item.meta.next];
    };
    LinkedList.prototype.contains = function (item) {
        if (!this.idFirst) {
            return false;
        }
        var node = this.first();
        while (node) {
            if (item === node) {
                return true;
            }
            node = this.next(node);
        }
        return false;
    };
    // Inefficient O(n) (worst case) linear search to find the position in the
    // list of a given Node. In production we would use a mapping or store the
    // node position inside the Node's meta data.
    LinkedList.prototype.position = function (item) {
        if (!this.idFirst) {
            return null;
        }
        var node = this.first();
        var n = 0;
        while (node) {
            if (item === node) {
                return n;
            }
            node = this.next(node);
            n++;
        }
        return null;
    };
    LinkedList.prototype.swap = function (a, b) {
        // Oh, you:
        if (a.meta.id === b.meta.id) {
            return;
        }
        // If a -> b:
        if (a.meta.next === b.meta.id) {
            this.remove(a);
            this.insertAfter(b, a);
            return;
        }
        // b -> a
        if (b.meta.next === a.meta.id) {
            this.remove(b);
            this.insertAfter(a, b);
            return;
        }
        // Otherwise they have at least 1 between them:
        if (this.idLast === a.meta.id) {
            this.idLast = b.meta.id;
        }
        else if (this.idLast === b.meta.id) {
            this.idLast = a.meta.id;
        }
        if (this.idFirst === a.meta.id) {
            this.idFirst = b.meta.id;
        }
        else if (this.idFirst === b.meta.id) {
            this.idFirst = a.meta.id;
        }
        var pPrev = a.meta.prev;
        var pNext = a.meta.next;
        a.meta.prev = b.meta.prev;
        a.meta.next = b.meta.next;
        b.meta.prev = pPrev;
        b.meta.next = pNext;
        // (ap) <-> (a) <-> (an/bp) <-> (b) <-> (bn)
        if (a.meta.prev) {
            this.nodes[a.meta.prev].meta.next = a.meta.id;
        }
        if (a.meta.next) {
            this.nodes[a.meta.next].meta.prev = a.meta.id;
        }
        if (b.meta.prev) {
            this.nodes[b.meta.prev].meta.next = b.meta.id;
        }
        if (b.meta.next) {
            this.nodes[b.meta.next].meta.prev = b.meta.id;
        }
    };
    /**
     * Bubble sort :)
     * Purely for education porpoises.
     * In real life I would not write my own sort routine.
     */
    LinkedList.prototype.bubble = function (comparator) {
        var node = this.first(), next, compareResult;
        var changedAny = false;
        while (node) {
            next = this.next(node);
            if (next) {
                compareResult = comparator(node, next);
                // Do nothing if compareResult < 0.
                // This one is already lower than next one.
                if (compareResult > 0) {
                    this.swap(node, next);
                    next = node;
                    changedAny = true;
                }
            }
            node = next;
        }
        return changedAny;
    };
    LinkedList.prototype.sort = function (comparator) {
        var changedAny = this.bubble(comparator);
        while (changedAny) {
            changedAny = this.bubble(comparator);
        }
    };
    return LinkedList;
}());
exports.LinkedList = LinkedList;
;
