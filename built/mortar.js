"use strict";
/**
 *
 *   /\     |\==/|
 *  /  \    \ == /
 *  |  |     |  |
 *  |  |     |  |
 * / == \    \  /
 * |/==\|     \/
 *
 *
 * Type definitions for a simple Mortar that can be launched from any given
 * height, but which always impacts the ground found at height 0.
 *
 * That is, the player is standing on a pedestal which could be from 0m tall up
 * to some arbitrary max height.  The floor around the pedestal is height 0.
 *
 * A real system would include collisions etc.  This is just a simple demo.
 *
 * Note: This is for education purposes only, and intentionally designed so
 * that calculations are only done during object initialization. That means the
 * "calc" functions expect you to never have changed the initial conditions. In
 * a real system, if the caller calls a calc function then that person probably
 * expects actual calculations to happen (for example, gravity has changed in
 * your world or the mortar got a vector affect from some force).
 *
 * g = gravity, const
 * t = time in seconds
 * h = initial height, const
 * vi = initial velocity, const
 *
 * Equation for vertical position at time (t) for a straight-vertical launch is:
 * p(t) = g*t^2 + vi*t + h
 *
 * Derivative of that gives us velocity at a given time(t):
 * v(t) = p'(t) = 2*g*t + vi
 *
 * Derivative of that gives us acceleration at a given time (t), and since
 * this is a mortar and not a rocket, there is only the acceleration due
 * to gravity, and it will be a constant:
 *
 * a(t) = p''(t) = 2*g
 *
 * Once you introduce a launch angle in radians(from 0 to pi/2), it lowers max
 * height and increases horizontal distance above zero.
 *
 * Given a launch angle A, you can take position function and multiply by
 * cos(A) to get X and sin(A) to get Y.
 *
 * Check out mortar.spec.js for some fun examples.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mortar = void 0;
var Mortar = /** @class */ (function () {
    function Mortar(initialVelocity, launchAngle, startingHeight, _gravity) {
        this.launchAngle = launchAngle;
        this.initialVelocity = initialVelocity;
        this.startingHeight = startingHeight;
        // For eventual player power-ups, let some mortars be affected by gravity-lessening effects.
        this.gravity = _gravity || -9.8;
        this.impactSeconds = this.calcImpactSeconds();
        this.maxHeight = this.calcMaxHeight();
        this.maxDistance = this.calcMaxDistance();
    }
    /**
     * When the position function intersects the x axis ("the floor"), we have impact.
     * This function returns the time at which that will happen.
     */
    Mortar.prototype.calcImpactSeconds = function () {
        // memoize since this call is expensive
        if (this.impactSeconds) {
            return this.impactSeconds;
        }
        var a = this.gravity / 2.0; // take 2nd integral of -9.8, you get (-9.8 * x^2 / 2).  here a is the coefficient, aka gravity/2.
        var b = this.initialVelocity * Math.sin(this.launchAngle);
        var c = this.startingHeight;
        // Set position function to 0 to get x intercepts. Solve w/ quadratic:
        var discriminant = (Math.sqrt(b * b - (4 * a * c)));
        var qe = ((-b) - discriminant) / (2.0 * a);
        if (qe < 0) {
            qe = ((-b) + discriminant) / (2.0 * a);
        }
        return qe;
    };
    Mortar.prototype.calcHeightAtTime = function (t) {
        return (this.gravity / 2.0) * t * t + this.initialVelocity * t * Math.sin(this.launchAngle) + this.startingHeight;
    };
    Mortar.prototype.calcDistanceAtTime = function (t) {
        return this.initialVelocity * t * Math.cos(this.launchAngle);
    };
    Mortar.prototype.calcMaxHeight = function () {
        if (this.maxHeight) {
            return this.maxHeight;
        }
        // when vertical velocity is zero, it is at its peak
        var timeAtNoVerticalVelocity = -(this.initialVelocity * Math.sin(this.launchAngle) / this.gravity);
        return this.calcHeightAtTime(timeAtNoVerticalVelocity);
    };
    Mortar.prototype.calcMaxDistance = function () {
        if (this.maxDistance) {
            return this.maxDistance;
        }
        var t = this.calcImpactSeconds();
        return this.calcDistanceAtTime(t);
    };
    return Mortar;
}());
exports.Mortar = Mortar;
