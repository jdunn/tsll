"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Node = exports.NodeMeta = void 0;
/**
 * Type definitions for a "node", which is any element in a linked list.
 */
var uuid_1 = require("uuid");
var NodeMeta = /** @class */ (function () {
    function NodeMeta() {
        this.id = uuid_1.v4(); // Would not actually use a 36 byte identity in prod unless there was a good reason to.
        // JS indexes can be numbers or strings.
        this.prev = null;
        this.next = null;
    }
    return NodeMeta;
}());
exports.NodeMeta = NodeMeta;
;
var Node = /** @class */ (function () {
    function Node(content) {
        this.meta = new NodeMeta();
        this.content = content;
    }
    return Node;
}());
exports.Node = Node;
