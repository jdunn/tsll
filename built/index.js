"use strict";
// Simple script which looks at the folders until ./node_modules/
// and adds them to a linked list.
// Sorts by name and prints, then sorts by mtime and prints.
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var list_1 = require("./list");
var node_1 = require("./node");
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var l = new list_1.LinkedList();
function populateList(ll, dir) {
    var files = fs_1.default.readdirSync(dir);
    files.forEach(function (file) {
        var fstats = fs_1.default.statSync(path_1.default.join(dir, file.toString()));
        var n = new node_1.Node({
            name: file.toString(),
            mtime: fstats.mtime
        });
        console.log("adding " + file.toString() + " : " + fstats.mtime);
        ll.addToBack(n);
    });
}
populateList(l, './node_modules/');
console.log("Unsorted:");
console.log(l.toString());
l.sort(function (a, b) {
    if (a.content.name > b.content.name) {
        return 1;
    }
    if (a.content.name < b.content.name) {
        return -1;
    }
    return 0;
});
console.log("Sorted by Name:");
console.log(l.toString());
l.sort(function (a, b) {
    if (a.content.mtime > b.content.mtime) {
        return 1;
    }
    if (a.content.mtime < b.content.mtime) {
        return -1;
    }
    return 0;
});
console.log("Sorted by Modify Time:");
console.log(l.toString());
