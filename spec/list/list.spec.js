/**
 * Spec file for LinkedList.  Tests are run using Jasmine.
 * See package.json for exec command.
 * 
 * Tests adding, removing, swapping, sorting, and finding nodes.
 */

// This ugliness is because
// 1. We have to run Jasmine in ES5 syntax
// 2. The CWD is spec/list, so we need relative include paths
const { LinkedList } = require('../../built/list');
const { Node } = require('../../built/node');
  
describe("LinkedList", () => {
    it("starts with 0 elements", () => {
        const l = new LinkedList({fun: false});
        expect(l.length()).toBe(0);
    });
})

function spawnRandomNodes(count) {
    let nodes = [];
    let c = 0;
    let n;
    while(c < count) {
        n = new Node({
            number: c,
            name: `creative_node_name_#${c}`,
            rand: 1 + Math.floor(Math.random()*100000.0)
        });
        nodes.push(n);
        c++;
    }
    return nodes;
}

describe("addToBack", () => {
    it("has no prev and next on first element", () => {
        const l = new LinkedList({fun: true});
        const n = new Node({blank: true});
        expect(l.length()).toBe(0);
        l.addToBack(n);
        expect(l.length()).toBe(1);
        expect(n.meta.id).not.toBe(null);
        expect(n.meta.prev).toBeNull();
        expect(n.meta.next).toBeNull();
    });
    it("orders correctly", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(3);
        l.addToBack(nodes[0]);
        expect(l.length()).toBe(1);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.last().meta.id).toBe(nodes[0].meta.id);

        l.addToBack(nodes[1]);
        expect(l.length()).toBe(2);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.last().meta.id).toBe(nodes[1].meta.id);

        l.addToBack(nodes[2]);
        expect(l.length()).toBe(3);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.first().meta.next).toBe(nodes[1].meta.id);
        expect(l.last().meta.id).toBe(nodes[2].meta.id);
    });
});

describe("addToFront", () => {
    it("has no prev and next on first element", () => {
        const l = new LinkedList({fun: true});
        const n = new Node({blank: true});
        expect(l.length()).toBe(0);
        l.addToFront(n);
        expect(l.length()).toBe(1);
        expect(n.meta.id).not.toBe(null);
        expect(n.meta.prev).toBeNull();
        expect(n.meta.next).toBeNull();
    });
    it("orders correctly", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(3);
        l.addToFront(nodes[0]);
        expect(l.length()).toBe(1);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.last().meta.id).toBe(nodes[0].meta.id);

        l.addToFront(nodes[1]);
        expect(l.length()).toBe(2);
        expect(l.first().meta.id).toBe(nodes[1].meta.id);
        expect(l.last().meta.id).toBe(nodes[0].meta.id);

        l.addToFront(nodes[2]);
        expect(l.length()).toBe(3);
        expect(l.first().meta.id).toBe(nodes[2].meta.id);
        expect(l.first().meta.next).toBe(nodes[1].meta.id);
        expect(l.last().meta.id).toBe(nodes[0].meta.id);
    });
});

describe("remove", () => {
    it("can remove the first item", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(3);
        nodes.forEach(n => { l.addToBack(n); })
        expect(l.length()).toBe(3);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.last().meta.id).toBe(nodes[2].meta.id);

        expect(l.remove(nodes[0])).toBe(true);
        expect(l.length()).toBe(2);
        expect(l.first().meta.id).toBe(nodes[1].meta.id);
        expect(l.last().meta.id).toBe(nodes[2].meta.id);
    });
    it("can remove the last item", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(3);
        nodes.forEach(n => { l.addToBack(n); })
        expect(l.length()).toBe(3);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.last().meta.id).toBe(nodes[2].meta.id);

        expect(l.remove(nodes[2])).toBe(true);
        expect(l.length()).toBe(2);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.last().meta.id).toBe(nodes[1].meta.id);
    });
    it("can remove a middle item", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(3);
        nodes.forEach(n => { l.addToBack(n); })
        expect(l.length()).toBe(3);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.last().meta.id).toBe(nodes[2].meta.id);

        expect(l.remove(nodes[1])).toBe(true);
        expect(l.length()).toBe(2);
        expect(l.first().meta.id).toBe(nodes[0].meta.id);
        expect(l.last().meta.id).toBe(nodes[2].meta.id);
    });
    it("will refuse to remove an item it does not know about", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(2);
        l.addToBack(nodes[0]);
        expect(l.length()).toBe(1);
        expect(l.remove(nodes[1])).toBe(false);
    });
});

describe("contains", () => {
    it("does not find an item until it is in the list", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(1);
        expect(l.contains(nodes[0])).toBe(false);
        l.addToBack(nodes[0]);
        expect(l.contains(nodes[0])).toBe(true);
    });
    it("will find an item until it is removed", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(3);
        nodes.forEach(n => {
            expect(l.contains(n)).toBe(false);
            l.addToBack(n);
            expect(l.contains(n)).toBe(true);
        })
        nodes.forEach(n => {
            expect(l.contains(n)).toBe(true);
            expect(l.remove(n)).toBe(true);
            expect(l.contains(n)).toBe(false);
        })
        expect(l.length()).toBe(0);
        expect(l.first()).toBe(null);
        expect(l.last()).toBe(null);
    });
});

describe("swap", () => {
    it("swaps two middle nodes with each other", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(4);
        nodes.forEach(n => { l.addToBack(n); })

        let expectedOrder = [0, 1, 2, 3];
        let eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })

        l.swap(nodes[1], nodes[2]);


        expectedOrder = [0, 2, 1, 3];
        eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })
    });
    it("swaps end of list with immediate predecessor", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(4);
        nodes.forEach(n => { l.addToBack(n); })

        l.swap(nodes[2], nodes[3]);

        let expectedOrder = [0, 1, 3, 2];
        let eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })
        expect(l.last().meta.id).toBe(nodes[2].meta.id);
    });
    it("swaps start of list with successor", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(4);
        nodes.forEach(n => { l.addToBack(n); })

        l.swap(nodes[0], nodes[1]);

        let expectedOrder = [1, 0, 2, 3];
        let eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })
        expect(l.first()).toBe(nodes[1]);
    });
    it("can swap the ends", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(4);
        nodes.forEach(n => { l.addToBack(n); })

        l.swap(nodes[0], nodes[3]);

        let expectedOrder = [3, 1, 2, 0];
        let eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })
        expect(l.first()).toBe(nodes[3]);
        expect(l.last()).toBe(nodes[0]);
    });
});

describe("sort", () => {
    it("can sort by name compare", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(5);
        nodes.forEach(n => { l.addToBack(n); })
        const compareByName = (a, b) => {
            if(a.content.name > b.content.name) return 1;
            if(a.content.name < b.content.name) return -1;
            return 0;
        }
        // sort by name
        l.sort(compareByName);
        let expectedOrder = [0, 1, 2, 3, 4];
        let eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })

        // reverse
        l.sort((a, b) => { return -1 * compareByName(a, b) });
        expectedOrder = [4, 3, 2, 1, 0];
        eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })
    });
    it("can sort by number compare", () => {
        const l = new LinkedList({fun: true});
        const nodes = spawnRandomNodes(5);
        let notSoRandom = 0;
        nodes.forEach(n => {
            l.addToBack(n);
            n.content.rand = notSoRandom;
            notSoRandom++;
        })
        const compareByNumber = (a, b) => {
            if(a.content.rand > b.content.rand) return 1;
            if(a.content.rand < b.content.rand) return -1;
            return 0;
        }
        // sort by name
        l.sort(compareByNumber);
        let expectedOrder = [0, 1, 2, 3, 4];
        let eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })

        // reverse
        l.sort((a, b) => { return -1 * compareByNumber(a, b) });
        expectedOrder = [4, 3, 2, 1, 0];
        eoIndex = 0;
        nodes.forEach(n => {
            expect(l.position(n)).toBe(expectedOrder[eoIndex]);
            eoIndex++;
        })
    });

})