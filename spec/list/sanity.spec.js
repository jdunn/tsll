/**
 * Spec file for very basic sanity checks.
 * These prove that Jasmine can successfully run tests
 * and that 
 */

describe("Sanity checks", function() {
    it("contains spec with an expectation", () => {
        expect(true).toBe(true);
    });
    it("we can rely on the typeof operator", () => {
        expect(typeof "fun").toBe('string');
    });
    it("can check the result of an ES6 syntax arrow function", () => {
        expect((() => 'party')()).toBe('party');
    });
});
