/**
 * Spec file for launching mortars and sorting based on various aspects.
 */

const { LinkedList } = require('../../built/list');
const { Node } = require('../../built/node');
const { Mortar } = require('../../built/mortar');

function spawnRandomMortars(count) {
    let nodes = [];
    let c = 0;
    let n;
    let mortar;
    while(c < count) {
        // random initial velocity from 1.0 to 100.0
        // random initial angle from 0 to 90
        // random initial height from 0 to 100
        mortar = new Mortar(1.0 + (Math.random() * 100.0), (Math.random() * Math.PI/2 ), Math.floor((Math.random() * 101.0)));
        n = new Node(mortar);
        nodes.push(n);
        c++;
    }
    return nodes;
}

// Number.EPSILON is *very* small, and using it to compare floats can lead to errors.
// Try it on sufficiently small float substractions and you'll see.
// So we define our own epsilon:
const OUR_EPSILON = 0.01;  // Very low resolution for demo purposes

function basicallyEquals(float1, float2) {
    return (Math.abs(float1 - float2) < OUR_EPSILON);
}
function expectFloatsToEqual(f1, f2) {
    expect(basicallyEquals(f1, f2)).withContext(`expected ${f1} and ${f2} to be within ${OUR_EPSILON} of each other.  Instead they are ${Math.abs(f1 - f2)} apart`).toBeTrue();
}

describe("max height, distance, and impact seconds", () => {
    it("a mortar fired straight up at a known velocity have known results", () => {
        // These consts are from known solutions calculated outside of the source.
        let m = new Mortar(100.0, Math.PI/2, 0);
        expectFloatsToEqual(m.calcMaxHeight(), 510.20);
        expectFloatsToEqual(m.calcMaxDistance(), 0.0);
        expectFloatsToEqual(m.calcHeightAtTime(0), 0);
        expectFloatsToEqual(m.calcImpactSeconds(), 20.40);
    });
    it("same mortar fired 10 feet off the ground", () => {
        // Same velocity and angle but starts 10m off the ground:
        m = new Mortar(100.0, Math.PI/2, 10);
        expectFloatsToEqual(m.calcMaxHeight(), 520.20);
        expectFloatsToEqual(m.calcMaxDistance(), 0.0);
        expectFloatsToEqual(m.calcImpactSeconds(), 20.50);
        expectFloatsToEqual(m.calcHeightAtTime(m.calcImpactSeconds()), 0);
    });
    it("45 degree, ground level", () => {
        m = new Mortar(100.0, Math.PI/4, 0);
        expectFloatsToEqual(m.calcMaxHeight(), 255.10);
        expectFloatsToEqual(m.calcMaxDistance(), 1020.40);
        expectFloatsToEqual(m.calcImpactSeconds(), 14.43);
        expectFloatsToEqual(m.calcHeightAtTime(m.calcImpactSeconds()), 0);
    });
    it("45 degree, 100 feet above the ground", () => {
        m = new Mortar(100.0, Math.PI/4, 100);
        expectFloatsToEqual(m.calcMaxHeight(), 355.10);
        expectFloatsToEqual(m.calcMaxDistance(), 1112.16);
        expectFloatsToEqual(m.calcImpactSeconds(), 15.72);
        expectFloatsToEqual(m.calcHeightAtTime(m.calcImpactSeconds()), 0);
    });
});

describe("a bunch of mortars in a list", () => {
    it("can sort by impact seconds", () => {
        const ll = new LinkedList();
        const mortars = spawnRandomMortars(100);
        mortars.forEach(m => ll.addToBack(m));
        ll.sort((a, b) => {
            return a.content.calcImpactSeconds() - b.content.calcImpactSeconds();
        });
        mortars.forEach(m => {
            const next = ll.next(m);
            if(next) {
                expect(m.content.calcImpactSeconds() < next.content.calcImpactSeconds());
            } else {
                expect(ll.last()).toBe(m);
            }
        })
        // If you're curious:
        // console.log(ll.toString());
    });
});
