/**
 * Type definitions for a "node", which is any element in a linked list.
 */
import { v4 as uuidv4 } from 'uuid';


export declare type NodeID = string;
export declare type OptionalNodeID = string|null;
export class NodeMeta {
    id: NodeID;
    prev: OptionalNodeID;
    next: OptionalNodeID;
    content: any;
    constructor() {
        this.id = uuidv4();  // Would not actually use a 36 byte identity in prod unless there was a good reason to.
                             // JS indexes can be numbers or strings.
        this.prev = null;
        this.next = null;
    }
};

export class Node {
    meta: NodeMeta;
    content: any;

    constructor(content: any) {
        this.meta = new NodeMeta();
        this.content = content;
    }
}