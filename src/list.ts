/**
 * Type definitions and class for a linked list.
 */

import { Node, NodeID, OptionalNodeID } from './node';

export type NodeStorage = { [id: string]: Node };

export class LinkedList {
    nodes: NodeStorage;
    idFirst: NodeID | null;
    idLast: NodeID | null;

    constructor() {
        this.nodes   = {};
        this.idFirst = null;
        this.idLast  = null;
    }
    length(): number {
        return Object.keys(this.nodes).length;
    }
    toString(): string {
        let n = this.first();
        let s = `\n----------------------------\n`;
        while(n) {
            s += JSON.stringify(n.content) + `\n`;
            n = this.next(n);
        }
        return s;
    }

    putBetween(prev: OptionalNodeID, id: NodeID, next: OptionalNodeID): void {
        this.nodes[id].meta.prev = prev;
        this.nodes[id].meta.next = next;
        if(next) {
            this.nodes[next].meta.prev = id;
        } else {
            this.idLast = id;
        }
        if(prev) {
            this.nodes[prev].meta.next = id;
        } else {
            this.idFirst = id;
        }
    }
    insertAfter(after: Node, item: Node): NodeID {
        this.nodes[item.meta.id] = item;
        this.putBetween(after.meta.id, item.meta.id, after.meta.next);
        return item.meta.id;
    }
    insertBefore(item: Node, before: Node): NodeID {
        this.nodes[item.meta.id] = item;
        this.putBetween(before.meta.prev, item.meta.id, before.meta.id);
        return item.meta.id;
    }
    addOnly(item: Node): NodeID {
        this.nodes[item.meta.id] = item;
        this.idFirst = item.meta.id;
        this.idLast = item.meta.id;
        return this.idFirst;
    }

    /**
     * "Back" of the list (amusement park line) means after the last one.
     */
    addToBack(item: Node): NodeID {
        if(this.idLast) {
            return this.insertAfter(this.nodes[this.idLast], item);
        }
        return this.addOnly(item);
    }
    addToFront(item: Node): NodeID {
        if(this.idFirst) {
            return this.insertBefore(item, this.nodes[this.idFirst]);
        }
        return this.addOnly(item);
    }
    remove(item: Node): boolean {
        if( ! this.nodes.hasOwnProperty(item.meta.id) ) {
            return false;
        }
        if(item.meta.prev) {
            this.nodes[item.meta.prev].meta.next = item.meta.next;
        } else {
            this.idFirst = item.meta.next;
        }
        if(item.meta.next) {
            this.nodes[item.meta.next].meta.prev = item.meta.prev;
        } else {
            this.idLast = item.meta.prev;
        }
        delete this.nodes[item.meta.id];
        return true;
    }
    first(): Node | null {
        if(this.idFirst) {
            return this.nodes[this.idFirst];
        }
        return null;
    }
    last(): Node | null {
        if(this.idLast) {
            return this.nodes[this.idLast];
        }
        return null;
    }
    next(item: Node): Node | null {
        if( ! item ) { return null; }
        if( ! item.meta.next ) { return null; }
        return this.nodes[item.meta.next];
    }
    contains(item: Node): boolean {
        if( ! this.idFirst ) { return false; }
        let node = this.first();
        while(node) {
            if(item === node) { return true; }
            node = this.next(node);
        }
        return false;
    }

    // Inefficient O(n) (worst case) linear search to find the position in the
    // list of a given Node. In production we would use a mapping or store the
    // node position inside the Node's meta data.
    position(item: Node): number|null {
        if( ! this.idFirst ) { return null; }
        let node = this.first();
        let n = 0;
        while(node) {
            if(item === node) { return n; }
            node = this.next(node);
            n++;
        }
        return null;
    }

    swap(a: Node, b: Node) {
        // Oh, you:
        if(a.meta.id === b.meta.id) { return; }

        // If a -> b:
        if(a.meta.next === b.meta.id) {
            this.remove(a);
            this.insertAfter(b, a);
            return;
        }
        // b -> a
        if(b.meta.next === a.meta.id) {
            this.remove(b);
            this.insertAfter(a, b);
            return;
        }

        // Otherwise they have at least 1 between them:

        if(this.idLast === a.meta.id) { this.idLast = b.meta.id; }
        else if(this.idLast === b.meta.id) { this.idLast = a.meta.id; }

        if(this.idFirst === a.meta.id) { this.idFirst = b.meta.id; }
        else if(this.idFirst === b.meta.id) { this.idFirst = a.meta.id; }

        const pPrev = a.meta.prev;
        const pNext = a.meta.next;

        a.meta.prev = b.meta.prev;
        a.meta.next = b.meta.next;

        b.meta.prev = pPrev;
        b.meta.next = pNext;

        // (ap) <-> (a) <-> (an/bp) <-> (b) <-> (bn)
        if(a.meta.prev) {
            this.nodes[a.meta.prev].meta.next = a.meta.id;
        }
        if(a.meta.next) {
            this.nodes[a.meta.next].meta.prev = a.meta.id;
        }

        if(b.meta.prev) {
            this.nodes[b.meta.prev].meta.next = b.meta.id;
        }
        if(b.meta.next) {
            this.nodes[b.meta.next].meta.prev = b.meta.id;
        }
    }
    /**
     * Bubble sort :)
     * Purely for education porpoises.
     * In real life I would not write my own sort routine.
     */
    bubble(comparator: (a: Node, b: Node) => number): boolean {
        let node = this.first(), next, compareResult;
        let changedAny = false;
        while(node) {
            next = this.next(node);
            if(next) {
                compareResult = comparator(node, next);
                // Do nothing if compareResult < 0.
                // This one is already lower than next one.
                if(compareResult > 0) {
                    this.swap(node, next);
                    next = node;
                    changedAny = true;
                }
            }
            node = next;
        }
        return changedAny;
    }
    sort(comparator: (a: Node, b: Node) => number): void {
        let changedAny = this.bubble(comparator);
        while(changedAny) {
            changedAny = this.bubble(comparator);
        }
    }
};
