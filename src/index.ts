
// Simple script which looks at the folders until ./node_modules/
// and adds them to a linked list.
// Sorts by name and prints, then sorts by mtime and prints.


import { LinkedList } from './list';
import { Node } from './node';
import fs from 'fs';
import path from 'path';

let l = new LinkedList();

function populateList(ll: LinkedList, dir: string) {
    const files = fs.readdirSync(dir);
    files.forEach(file =>  {
        const fstats = fs.statSync(path.join(dir, file.toString()));
        let n = new Node({
            name: file.toString(),
            mtime: fstats.mtime
        });
        console.log(`adding ${file.toString()} : ${fstats.mtime}`)
        ll.addToBack(n);
    });
}

populateList(l, './node_modules/');

console.log(`Unsorted:`);
console.log(l.toString());

l.sort((a, b) => {
    if(a.content.name > b.content.name) { return 1; }
    if(a.content.name < b.content.name) { return -1; }
    return 0;
})
console.log(`Sorted by Name:`);
console.log(l.toString());

l.sort((a, b) => {
    if(a.content.mtime > b.content.mtime) { return 1; }
    if(a.content.mtime < b.content.mtime) { return -1; }
    return 0;
})
console.log(`Sorted by Modify Time:`);
console.log(l.toString());
